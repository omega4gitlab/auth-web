const VueTemplateCompiler = require("vue-template-compiler");

const findTagByRegexp = function (str, regexp, pkgTagFn, insert) {
    if (!regexp.global) {
        //regexp必须是全局正则表达式，否则返回null
        return null;
    }
    var ret = [];
    var pointer = 0;
    var item;
    while ((item = regexp.exec(str)) != null) {
        var endIndex = item.index + item[0].length;
        var prevStr = "";
        if (item.index > pointer) {
            prevStr = str.slice(pointer, item.index);
        }
        if (insert) {
            prevStr += item[0];
        }
        if (prevStr) {
            ret.push(prevStr);
        }
        var matches = [];
        for (var i = 0; i < item.length; i++) {
            matches[i] = item[i + 1];
        }
        ret.push(pkgTagFn(matches, item[0]));
        pointer = endIndex;
    }
    if (pointer != str.length) {
        ret.push(str.slice(pointer));
    }
    return ret;
};

module.exports = function (opt) {
    if (opt && opt.classPrefix) {
        var newCompiler = Object.create(VueTemplateCompiler);
        newCompiler.compile = function (template, options) {
            var arr = findTagByRegexp(template, /:?class=([\'\"])(.*?)\1/gi, function (matches, str) {
                var classes = matches[1];
                if (":" == str[0]) {
                    return ":class=\"addClassPrefix(" + classes + ",'comp-')\"";
                } else {
                    var newClasses = classes.split(/\s+/).map(function (item) {
                        return opt.classPrefix + item;
                    }).join(" ");
                    return "class=\"" + newClasses + "\"";
                }
            }, false);
            var newTemplate = arr.join("");
            return VueTemplateCompiler.compile(newTemplate, options);
        };
        return newCompiler;
    } else {
        return VueTemplateCompiler;
    }
};