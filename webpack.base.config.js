"use strict"

const { join, resolve } = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const { VueLoaderPlugin } = require("vue-loader");
const VueTemplateCompilerHack = require("./VueTemplateCompilerHack.js");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

const BaseConfigFactory = function (env, argv) {

  const config = {
    entry: {
      index: env.SRC + "/index.js"
    },
    resolve: {
      extensions: [".js", ".ts", ".vue"]
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: "vue-loader",
          options: {
            loaders: {
              css: ["css-hot-loader", MiniCssExtractPlugin.loader, "css-loader"],
              less: ["css-hot-loader", MiniCssExtractPlugin.loader, "css-loader", "postcss-loader", "less-loader"],
              scss: ["css-hot-loader", MiniCssExtractPlugin.loader, "css-loader", "postcss-loader", "sass-loader"],
              postcss: ["css-hot-loader", MiniCssExtractPlugin.loader, "css-loader", "postcss-loader"]
            },
            // compiler: VueTemplateCompilerHack({
            //     classPrefix: "comp-"
            // })
          }
        },
        {
          test: /\.ts$/,
          use: ["babel-loader", {
            loader: "ts-loader",
            options: {
              appendTsSuffixTo: [/\.vue$/],
              transpileOnly: true
            }
          }]
        },
        {
          test: /\.js$/,
          use: "babel-loader",
          exclude: /node_modules/
        },
        {
          test: /\.css$/,
          use: ["css-hot-loader", MiniCssExtractPlugin.loader, "css-loader", "postcss-loader"]
        },
        {
          test: /\.less$/,
          use: ["css-hot-loader", MiniCssExtractPlugin.loader, "css-loader", "postcss-loader", "less-loader"]
        },
        {
          test: /\.scss$/,
          use: ["css-hot-loader", MiniCssExtractPlugin.loader, "css-loader", "postcss-loader", "sass-loader"]
        },
        {
          test: /\.html$/,
          use: [{
            loader: "html-loader",
            options: {
              root: resolve(__dirname, "src"),
              attrs: ["img:src", "link:href"]
            }
          }]
        },
        {
          test: /\.(png|jpg|jpeg|gif|svg|svgz)(\?.+)?$/,
          exclude: /favicon\.png$/,
          use: [{
            loader: "url-loader",
            options: {
              limit: 10000,
              name: "assets/img/[name].[hash:7].[ext]",
              esModule: false
            }
          }],
          type: "javascript/auto"
        },
        {
          test: /\.(eot|ttf|woff|woff2)(\?.+)?$/,
          use: [{
            loader: "url-loader",
            options: {
              limit: 10000,
              name: "assets/fonts/[name].[hash:7].[ext]",
              esModule: false
            }
          }],
          type: "javascript/auto"
        },
        {
          test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.+)?$/,
          loader: "url-loader",
          options: {
            limit: 10000,
            name: "assets/media/[name].[hash:7].[ext]",
            esModule: false
          },
          type: "javascript/auto"
        }
      ]
    },
    plugins: [
      new VueLoaderPlugin(),
      new CleanWebpackPlugin(),
      new MiniCssExtractPlugin()
    ]
  };

  return config;
};

module.exports = BaseConfigFactory;

