#!/usr/bin/env bash
#编译+部署项目脚本
 
#需要配置如下参数

set -x
su - root

WORKSPACE=$WORKSPACE

# 项目名称
PROJ_NAME=goose-auth-web

DEPLOY_IP=161.189.57.196

# 项目代码路径, 在Jenkins任务的Execute Shell中配置项目路径, pwd 就可以获得该项目路径
DEPLOY_PATH=/var/www/comps/auth

error_exit() {
	msg=$1
	echo $msg
	exit 1
}

build() {
	WORKSPACE=$1
  OUTPUT_FILE=$2
  cd $WORKSPACE
	npm install
#	npm --registry https://registry.npm.taobao.org install
	npm run build || error_exit "build failed!"
#  rm -rf $OUTPUT_FILE
  cd $WORKSPACE/dist
  git log -n 1 > ./version.txt
  zip -r $OUTPUT_FILE ./*
}

build_target_script() {
	SCRIPT_PATH=$1

cat > $SCRIPT_PATH << "EOF"
error_exit() {
  msg=$1
  echo $msg
  exit 1
}
prepare_path() {
  if [ ! -d "$1" ];then
    mkdir -pv $1
  fi
}
backup() {
  APP_ROOT=$1
  BACKUP_FILE=$2
  cd $APP_ROOT
#  rm -rf $BACKUP_FILE
  zip -r -m $BACKUP_FILE ./*
}
EOF

TMP_DIR=/home/root/tmp/
BACKUP_DIR=/home/root/backup/$(date "+%Y.%m.%d")/

cat >> $SCRIPT_PATH << EOF
update_app() {
  prepare_path $BACKUP_DIR
  backup $DEPLOY_PATH $BACKUP_DIR$PROJ_NAME$(date "+%Y.%m.%d.%H.%M.%S").zip
  prepare_path $DEPLOY_PATH
  unzip -o $TMP_DIR$PROJ_NAME/$PROJ_NAME.zip -d $DEPLOY_PATH
}
update_app
EOF

}


deploy_program(){

	# 复制zip包到远程服务器
	echo "ssh -v -o StrictHostKeyChecking=no root@$1" 
	ssh  -o StrictHostKeyChecking=no root@$1 "mkdir -p /home/root/tmp/$PROJ_NAME;rm -rf /home/root/tmp/$PROJ_NAME/*;"	
	echo "uploading zip..."	
	scp  -o StrictHostKeyChecking=no $WORKSPACE/$PROJ_NAME.zip root@$1:/home/root/tmp/$PROJ_NAME/

	ssh -o StrictHostKeyChecking=no root@$1 > /goose/ssh.log 2>&1 < $WORKSPACE/deploy.sh

}

# 编译项目
build $WORKSPACE $WORKSPACE/$PROJ_NAME.zip || error_exit "build failed!"

#生成远程服务器执行脚本（主要是将运行参数带过去）
build_target_script $WORKSPACE/deploy.sh

#循环发布到多台服务器
array=(${DEPLOY_IP//,/ })  
 
for var in ${array[@]}
do
	deploy_program $var
done

