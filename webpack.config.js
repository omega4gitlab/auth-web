"use strict"

const { join, resolve } = require("path");
const webpack = require("webpack");
const { merge } = require("webpack-merge");

const SRC = resolve(__dirname, "./src"); // 源文件目录
const DEST = resolve(__dirname, "./dist"); // 发布目录

const BaseConfigFactory = require("./webpack.base.config.js");

module.exports = function (env, argv) {
    if (!env) {
        env = {};
    }
    env.SRC = env.SRC || SRC;
    env.DEST = env.DEST || DEST;
    const config = merge(BaseConfigFactory(env, argv), {
        externals: {
            "vue": 'Vue',
            "vuex": 'Vuex',
            'vue-router': 'VueRouter',
            'element-ui': 'ELEMENT'
        },
        resolve: {
            alias: {
                '@': SRC
            }
        },
        output: {
            path: env.DEST,
            filename: "[name].js",
            chunkFilename: "[name].[chunkhash].js",
            publicPath: ""
        },
        devServer: {
            host: "127.0.0.1",
            port: 8010,
            historyApiFallback: false,
            devMiddleware: {
                writeToDisk: true,
            },
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": "true",
                "Access-Control-Allow-Methods": "GET, POST, OPTIONS",
                "Access-Control-Allow-Headers": "DNT,web-token,app-token,Authorization,Accept,Origin,Keep-Alive,User-Agent,X-Mx-ReqToken,X-Data-Type,X-Auth-Token,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range",
            },
        },
        devtool: "production" == argv.mode ? "source-map" : "eval-source-map"
    });
    return config;
};
