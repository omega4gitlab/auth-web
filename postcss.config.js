module.exports = {
    plugins: [
        require("autoprefixer")({
            "browsers": [
                "defaults",
                "not ie < 9",
                "last 2 versions",
                "> 1%"
            ]
        }),
        // require("postcss-prefixer")({
        //     prefix: "comp-",
        //     ignore: [/^#\S+$/],
        // })
    ]
}
